package org.nrg.xnat.build.hello;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class HelloApplicationTests {
    @Before
    public void initialize() {
        log.info("Initializing HelloApplicationTests");
    }

	@Test
	public void contextLoads() {
        log.info("Executing the HelloApplicationTests.contextLoads() test");
	}

}

