package org.nrg.xnat.build.hello;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class HelloApplication implements CommandLineRunner {
	public static void main(String[] args) {
        log.info("Now starting HelloApplication with {} args: {}", args.length, args);
		SpringApplication.run(HelloApplication.class, args);
	}

    @Override
    public void run(final String... args) {
        log.info("Executing HelloApplication.run() with {} args: {}", args.length, args);
        System.out.println("Well, hello there...");
    }
}

