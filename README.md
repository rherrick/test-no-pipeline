# Test Project without Pipeline #

This is a test project that doesn't have a <b>Jenkinsfile</b> defining a pipeline
to serve as a target for a pipeline with the workflow defined in the job configuration.

## Build Targets ##

The following build targets could be useful for testing various pipeline steps:

* **jar** builds the application jar
* **bootJar** builds an executable jar for the application
* **bootDistTar** builds a tar with a distributable application (includes scripts, etc.)
* **bootDistZip** builds a zip with a distributable application (includes scripts, etc.)
* **javadoc** builds the Javadocs
* **test** runs the unit tests
* **jacocoTestCoverageVerification** verifies coverage metrics (we'd like to fail builds with coverage less than _X_%)
* **jacocoTestReport** builds the Jacoco test report (we'd like to fail builds with coverage less than _X_%)
* **javadocCoverageReport** builds the Javadoc coverage report (we'd like to fail builds with coverage less than _X_%)

